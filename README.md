
<!-- README.md is generated from README.Rmd. Please edit that file -->

# README

(test\_from\_20176)

<!-- badges: start -->
<!-- badges: end -->

The goal of this repository is to document using SSH keys to connect
your RStduio with Duke’s OIT GitLab

1.  [Configure ssh keys on your
    RStudio](ssh_keys_and_config_for_gitlab.md)
2.  [Create a GitLab repo, locally connect/configure with RStudio
    project, git push](ssh_handson.md)
3.  You’re good now

Questions? Consult the more complete GitHub instructions to [generate
and add an SSH
key](https://help.github.com/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent/)

## Foo

``` r
summary(cars)
#>      speed           dist       
#>  Min.   : 4.0   Min.   :  2.00  
#>  1st Qu.:12.0   1st Qu.: 26.00  
#>  Median :15.0   Median : 36.00  
#>  Mean   :15.4   Mean   : 42.98  
#>  3rd Qu.:19.0   3rd Qu.: 56.00  
#>  Max.   :25.0   Max.   :120.00
```

You can also embed plots, for example:

![](README_files/figure-gfm/pressure-1.png)<!-- -->

In that case, don’t forget to commit and push the resulting figure
files, so they display on GitHub.
