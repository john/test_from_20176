# SSH & GitLab: how to create a GitLab repo that RStudio/Git can interact with

#### Generate SSH Keys 

This step was covered in the [Outline Section](ssh_keys_and_config_for_gitlab.md).  This must be done on each workstation you use to *push*.  


## Make a Repository
1.Login to OIT's Gitlab -- <a href="https://gitlab.oit.duke.edu" target="_blank">gitlab.oit.duke.edu</a>

1. New repository 

1. Add a repository name.  Use, e.g., the name *test-one*  <br> Type  'test-one' and add a short description

## GitLab

Make new RStudio project using the same name as the GitLab repo.
THEN, in the RStudio **terminal**  (need to have _git bash_ on Windows)

```
git init
git remote add origin git@gitlab.oit.duke.edu:john/test_from_20176.git  
# use the the gitlab address found within the "Push an existing folder" command line instructions
```

```
git add .
git commit -m "Initial commit"
git push -u origin master
```

> may get terminal prompt to verify the _authenticity of the host_ gitlab.oit.duke.ed , If so, I said "yes" to the question (Are you sure you want to continue connecting?)

Now, refresh on Gitlab -- the screen should change.  This verifies it's working

Now -- in the RStudio **console**

```
library(usethis)
use_git_ignore(c(".Rproj.user", ".Rhistory", 
                  ".Rdata", ".httr-oauth", ".DS_Store"))
use_readme.md()
use_ccby_license()
```

Restart RStudio

1) use Git tab (near  Environment tab)

